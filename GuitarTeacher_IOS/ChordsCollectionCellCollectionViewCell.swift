//
//  ChordsCollectionCellCollectionViewCell.swift
//  GuitarTeacher_IOS
//
//  Created by Ученик on 28.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit
//почему пишем @IBOutlet
//зачем UI и ! в UILabel!
class ChordsCollectionCellCollectionViewCell: UICollectionViewCell {
    //создаем переменую для связи эллементов на экране
    @IBOutlet var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 5
        layer.borderColor = UIColor(red: 2.0/255.0, green: 1.0/255.0, blue: 172.0/255.0, alpha:  0.4).cgColor
        layer.cornerRadius = 7
        layer.masksToBounds = true
        
//        backgroundColor = UIColor(red: 2.0/255.0, green: 1.0/255.0, blue: 172.0/255.0, alpha:  0.4)
    }
}
