//
//  ChordsTableViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Синицын on 07.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class ChordsTableViewController: UITableViewController {
    
    //создание массива типа String
    var nameLabel: [String] = ["Power и Квинт аккорды", "Остальные аккорды"]

    override func viewDidLoad() {
        super.viewDidLoad()
//        print("ChordsTableViewController viewDidLoad")
//        self.title = "Виды аккордов"
        
        test(arg: "")
        test1("")
        test2(qwer: "")
    }
    
    func test(arg: String) {
    }
    func test1(_ arg: String) {
    }
    func test2(qwer arg: String) {
        print(arg)
    }
    
//зачем писать override, _ tableView, numberOfRowsInSection section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //возарашение количества эллементов в массиве nameLabel
        return nameLabel.count
    }
    
//зачем писать cellForRowAt indexPath, почему возвращаемый тип UITableViewCell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //--- создание переменной cell в которой хранится индификатор label с индексом
//что присваевается переменной 
//зачем писать dequeueReusableCell cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath)
        //создается переменная menuCell, которой приваивается то что хранится в cell, как MenuCell
//зачем писать as?, зачем "?"
        if let menuCell = cell as? MenuCell {
        //в label выводится соответсвующий эллемент массива(по индексу)
//зачем .row
            menuCell.label.text = nameLabel[indexPath.row]
        }
        //возвращается cell
        return cell
    }
//зачем писать didSelectRowAt indexPath
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //создается перемнная которой присваивается пустая строка
        var nameIdentifier = ""
        // если элемент массива соответсвует "Power и Квинт аккорды"
        if nameLabel[indexPath.row] == "Power и Квинт аккорды" {
        // тогда перемнной nameIdentifier присваеивается индификатор "PowerTableViewController"
            nameIdentifier = "PowerTableViewController"
            
        //если элемент массива соответсвует "Остальные аккорды"
        } else if nameLabel[indexPath.row] == "Остальные аккорды" {
        // тогда перемнной nameIdentifier присваеивается индификатор "RestChordsTableViewController"
            nameIdentifier = "RestChordsTableViewController"
            
        }
        //создается переменная chordsViewController которая хранит в себе nameIdentifier
//зачем storyboard?.instantiateViewController
        if let chordsViewController = storyboard?.instantiateViewController(withIdentifier: nameIdentifier) {
        //показывается chordsViewController(переход)
//зачем sender: nil
            show(chordsViewController, sender: nil)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
