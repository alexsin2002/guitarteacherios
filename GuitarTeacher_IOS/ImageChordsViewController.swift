//
//  ImageChordsViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Ученик on 11.03.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class ImageChordsViewController: UIViewController {
    //создаем переменую для связи эллементов на экране
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    //создаем переменную с пустой строкой
    var chordPath = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentInsetAdjustmentBehavior = .never
        //Теперь представление для картинки равно по размеру картинке
        imageView.frame.size = (imageView.image?.size)!
        
        //обращение к image от элемента imageView. Прикрепляется картинка, название которой находится в переменой chordName
        setZoomParametersForSize()
        
        var fullPathArr = chordPath.split(separator: "/")
        if fullPathArr.count == 3 && fullPathArr[0] != "power chords" {
            chordPath = fullPathArr[0] + "/" + fullPathArr[1] + " дробь " + fullPathArr[2]
        }
        
        
        if let imageURL = Bundle.main.url(forResource: chordPath, withExtension: nil) {
            
            imageView.image = UIImage(contentsOfFile: imageURL.path)

            scrollView.contentSize = view.bounds.size
            scrollView.delegate = self
        }
    }
}

extension ImageChordsViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func setZoomParametersForSize() {
        //Получение минимального масштабирования
        let minScale = CGFloat(1)
        //Минимально возможное масштабирование
        scrollView.minimumZoomScale = minScale
        //Максимально возможное
        scrollView.maximumZoomScale = 2.5
        //Изначальное масштабирование
        scrollView.zoomScale = minScale
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scale == 1,
            !imageView.frame.origin.equalTo(.zero) {
            UIView.animate(withDuration: 0.3) {
                self.imageView.frame = self.imageView.bounds
                self.imageView.setNeedsLayout()
            }
        }
    }
}
