//
//  LessonsTableViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Синицын on 07.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class LessonsTableViewController: UITableViewController {
    // создается массив типа String
    var nameLabel: [String] = ["Гаммы", "Лесенка", "Паучок", "Расстяжка пальцев вдоль грифа", "Расстяжка пальцев поперек грифа", "Увелечение скорости игры"]

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.title = "Упражнения"
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //возвращается колличесвто элементов массива
        return nameLabel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //создается переменная cell, которой присваивают возвращаемое значение функции dequeueReusableCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath)
        //если ячейка класса menuCell, тогда присваевается переменной menuCell
        if let menuCell = cell as? MenuCell {
        //в label выводится соответсвующий эллемент массива(по индексу)
            menuCell.label.text = nameLabel[indexPath.row]
        }
        //возвращение cell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var text = ""
        //создается переменая, идет обращение в storyboard, идификатор TextViewController, затем идет проверка на принадлежность классу TextViewController
        if let TextViewController = storyboard?.instantiateViewController(withIdentifier: "TextViewController") as? TextViewController {
            //если пользователь нажал на кнопку "Гаммы"
            if nameLabel[indexPath.row] == "Гаммы" {
                text = Database.scales
                //если пользователь нажал на кнопку "Лесенка"
            } else if nameLabel[indexPath.row] == "Лесенка" {
                //массиву chords присваевается, то что хранится в массиве chordForFiveString
                text = Database.ladder
                
                //если пользователь нажал на кнопку "Паучок"
            } else if nameLabel[indexPath.row] == "Паучок" {
                //массиву chords присваевается, то что хранится в массиве chordForSixString
                text = Database.spider
            } else if nameLabel[indexPath.row] == "Расстяжка пальцев вдоль грифа" {
            //массиву chords присваевается, то что хранится в массиве chordForFiveString
            text = Database.fingertipAlongTheFingerboard
            
            //если пользователь нажал на кнопку "Расстяжка пальц поперек грифа"
        } else if nameLabel[indexPath.row] == "Расстяжка пальцев поперек грифа" {
            //массиву chords присваевается, то что хранится в массиве chordForSixString
            text = Database.fingertipAcrossTheNeck
        } else if nameLabel[indexPath.row] == "Увелечение скорости игры" {
            //массиву chords присваевается, то что хранится в массиве chordForSixString
            text = Database.increaseGameSpeed
        }
            
            if let viewController = storyboard?.instantiateViewController(withIdentifier: "TextViewController") {
                if let textController = viewController as? TextViewController {
                    textController.text = text
                }
            //показывается chordsViewController(переход)
            show(viewController, sender: nil)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
