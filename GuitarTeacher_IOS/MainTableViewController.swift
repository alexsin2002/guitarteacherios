//
//  MainTableViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Синицын on 07.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    //создание массива типа String
    var nameLabel: [String] = ["Аккорды", "Как читать табы", "Упражнения", "Что нужно для игры", "С чего начать"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //возарашение количества эллементов в массиве nameLabel
        return nameLabel.count
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //создание переменной cell в которой хранится то, что возвращает функция dequeueReusableCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath)
        //если ячейка класса menuCell, тогда присваевается переменной menuCell
        if let menuCell = cell as? MenuCell {
            //в label выводится соответсвующий эллемент массива(по индексу)
            menuCell.label.text = nameLabel[indexPath.row]
        }
        //возвращается cell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //создается перемнная которой присваивается пустая строка
        var nameIdentifier = ""
        var text = ""
        var title = ""
        // если элемент массива соответсвует "Аккорды"
        if nameLabel[indexPath.row] == "Аккорды" {
            // тогда перемнной nameIdentifier присваеивается индификатор "ChordsTableViewController"
            nameIdentifier = "ChordsTableViewController"
            
            // если элемент массива соответсвует "Упражнения"
        } else if nameLabel[indexPath.row] == "Упражнения" {
            // тогда перемнной nameIdentifier присваеивается индификатор "LessonsTableViewController"
            nameIdentifier = "LessonsTableViewController"
            // если элемент массива соответсвует "Как читать табы"
        } else if nameLabel[indexPath.row] == "Как читать табы" {
            title = "Табы"
            nameIdentifier = "TextViewController"
            text = Database.howReadTabs
            // если элемент массива соответсвует "Что нужно для игры"
        } else if nameLabel[indexPath.row] == "Что нужно для игры" {
            title = "Что нужно для игры"
            nameIdentifier = "TextViewController"
            text = Database.whatYouNeedPlay
            // если элемент массива соответсвует "С чего начать"
        } else if nameLabel[indexPath.row] == "С чего начать" {
            title = "С чего начать"
            nameIdentifier = "TextViewController"
            text = Database.whereBegin
        }
    // если в storyboard есть контроллер с индификатором ChordsTableViewController
        if let viewController = storyboard?.instantiateViewController(withIdentifier: nameIdentifier) {
            if let textController = viewController as? TextViewController {
                textController.text = text
                textController.title = title
            }
            //показывается chordsViewController(переход)
            show(viewController, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
