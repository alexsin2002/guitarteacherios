//
//  PicChordsCollectionViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Ученик on 28.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit
//создание переменной reuseIdentifier
private let reuseIdentifier = "Cell"

class PicChordsCollectionViewController: UICollectionViewController {
    //создание пустого массива
    var chords = [ChordModel] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "Список аккордов"
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //возвращение колличества элементов в массиве
        return chords.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //--- создается переменная cell которой присваивается индификатор reuseIdentifier и индекс label'а
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
         //если ячейка класса ChordsCollectionCellCollectionViewCell, тогда присваевается переменной chordsCollection
        if let chordsCollection = cell as? ChordsCollectionCellCollectionViewCell {
            //объявляем переменную типа ChordModel
            var chordModel: ChordModel
            //инициализируем переменную элементом из массива chords(по индексу)
            chordModel = chords[indexPath.row]
            //объявляем переменную типа String
            var title: String
            //инициализируем переменную тайтлом из переменной chordModel
            title = chordModel.title
            //в label выводится соответсвующий title эллемента переменной(по индексу)
            chordsCollection.label.text = title
        }
        //возвращается cell
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //если в storyboard есть контроллер с индификатором ImageChordsViewController, как ImageChordsViewController
        if let chordsViewController = storyboard?.instantiateViewController(withIdentifier: "ImageChordsViewController") as? ImageChordsViewController {
            var chordModel: ChordModel
            chordModel = chords[indexPath.row]
            var path: String
            path = chordModel.path
            //присваевается содиржимое массива в ячейке с номером ряда в таблице
                chordsViewController.chordPath = path
            //показывается chordsViewController(переход)
            show(chordsViewController, sender: nil)
        }
    }
    
    
}
