//
//  PowerChordsTableViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Синицын on 14.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class PowerChordsTableViewController: UITableViewController {
    //создание массива типа String
    var nameLabel: [String] = ["от 4-ой струны", "от 5-ой струны", "от 6-ой струны"]
    
    //D5, E5, F5, G5, A5, H5, C5 от 4-ой струны
    //A5, H5, C5, D5, E5, F5, G5 от 5-ой струны
    //E5, F5, G5, A5, H5, C5, D5 от 6-ой струны
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("ChordsTableViewController viewDidLoad")
//        self.title = "Power аккорды"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //возвращается колличество item в массиве
        return nameLabel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //--- создание переменной cell в которой хранится индекс label'а в таблице
        let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath)
        //если ячейка класса MenuCell, тогда присваевается переменной menuCell
        if let menuCell = cell as? MenuCell {
            //в label выводится соответсвующий эллемент массива(по индексу)
            menuCell.label.text = nameLabel[indexPath.row]
        }
        //возвращается переменная cell
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //создается переменая, идет обращение в storyboard, идификатор PicChordsCollectionViewController, затем идет проверка на принадлежность классу PicChordsCollectionViewController
        if let chordsViewController = storyboard?.instantiateViewController(withIdentifier: "PicChordsCollectionViewController") as? PicChordsCollectionViewController {
            //если пользователь нажал на кнопку "от 4-ой струны"
            if nameLabel[indexPath.row] == "от 4-ой струны" {
                chordsViewController.chords = Database.chordsForFourString
                //если пользователь нажал на кнопку "от 5-ой струны"
            } else if nameLabel[indexPath.row] == "от 5-ой струны" {
                //массиву chords присваевается, то что хранится в массиве chordForFiveString
                chordsViewController.chords = Database.chordsForFiveString
                
                //если пользователь нажал на кнопку "от 6-ой струны"
            } else if nameLabel[indexPath.row] == "от 6-ой струны" {
                //массиву chords присваевается, то что хранится в массиве chordForSixString
                chordsViewController.chords = Database.chordsForSixString
            }
            //показывается chordsViewController(переход)
            show(chordsViewController, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
