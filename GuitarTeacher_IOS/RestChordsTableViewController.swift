//
//  RestChordsTableViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Ученик on 28.02.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class RestChordsTableViewController: UITableViewController {
    //создание массива типа String в котором хранятся все типы аккордов
    var nameLabel: [String] = ["A(Ля)", "C(До)", "D(Ре)", "E(Ми)", "F(Фа)", "G(Соль)", "H(Си)"]
    
    
   // Database.A[0].image
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        print("ChordsTableViewController viewDidLoad")
//        self.title = "Остальные аккорды"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //возарашение количества эллементов в массиве nameLabel
        return nameLabel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //--- создание переменной cell в которой ранится индекс label'а в таблице
        let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath)
        //если ячейка класса MenuCell, тогда присваевается переменной menuCell
        if let menuCell = cell as? MenuCell {
            //в label выводится соответсвующий эллемент массива(по индексу)
            menuCell.label.text = nameLabel[indexPath.row]
        }
        //возвращается cell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //если в storyboard есть контроллер класса PicChordsCollectionViewController с индификатором PicChordsCollectionViewController
        if let chordsViewController = storyboard?.instantiateViewController(withIdentifier: "PicChordsCollectionViewController") as? PicChordsCollectionViewController {
            //если совападает "A(Ля)" с эллементом массива
            if nameLabel[indexPath.row] == "A(Ля)" {
                //переменной chords присвоятся эллементы массива chordA
                chordsViewController.chords = Database.A
                
                //если совападает "C(До)" с эллементом массива
            } else if nameLabel[indexPath.row] == "C(До)" {
                //переменной chords присвоятся эллементы массива chordC
                chordsViewController.chords = Database.C
                
                //если совападает "D(Ре)" с эллементом массива
            } else if nameLabel[indexPath.row] == "D(Ре)" {
                //переменной chords присвоятся эллементы массива chordD
                chordsViewController.chords = Database.D
                
                //если совападает "E(Ми)" с эллементом массива
            } else if nameLabel[indexPath.row] == "E(Ми)" {
                //переменной chords присвоятся эллементы массива chordE
                chordsViewController.chords = Database.E
                
                //если совападает "F(Фа)" с эллементом массива
            } else if nameLabel[indexPath.row] == "F(Фа)" {
                //переменной chords присвоятся эллементы массива chordF
                chordsViewController.chords = Database.F
                
                //если совападает "G(Соль)" с эллементом массива
            } else if nameLabel[indexPath.row] == "G(Соль)" {
                //переменной chords присвоятся эллементы массива chordG
                chordsViewController.chords = Database.G
                
                //если совападает "H(Си)" с эллементом массива
            } else if nameLabel[indexPath.row] == "H(Си)" {
                //переменной chords присвоятся эллементы массива chordH
                chordsViewController.chords = Database.H
            }
            //показывается chordsViewController(переход)
            show(chordsViewController, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
