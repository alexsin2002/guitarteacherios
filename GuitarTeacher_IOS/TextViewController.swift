//
//  TextViewController.swift
//  GuitarTeacher_IOS
//
//  Created by Ученик on 15.04.2019.
//  Copyright © 2019 Синицын. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {
    @IBOutlet var labelText: UILabel!
    
    var text = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelText.text = text
    }
    
}
